import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrunoLearningPixieComponent } from './lazyLoadedPages/bruno-learning-pixie/bruno-learning-pixie.component';
import { TaskGeckoComponent } from './lazyLoadedPages/task-gecko/task-gecko.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'task-gecko',
  },
  {
    path: 'BrunoLearningPixie',
    component: BrunoLearningPixieComponent,
    loadChildren: () =>
      import(
        './lazyLoadedPages/bruno-learning-pixie/bruno-learning-pixie.module'
      ).then((m) => m.BrunoLearningPixieModule),
  },
  {
    component: TaskGeckoComponent,
    path: 'task-gecko',
    loadChildren: () =>
      import('./lazyLoadedPages/task-gecko/task-gecko.module').then(
        (m) => m.TaskGeckoModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
