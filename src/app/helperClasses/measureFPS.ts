/**
 * Measure Frames Rate Per Second, by adding it to a tick function
 *
 * HOW TO USE: create new MeasureFPS on init, and add measureFPS.atTick() inside your tick function
 *
 * use measureFPS.FPS to display current FPS
 */

export class MeasureFPS {
  /**
   * @param intervalInMillis this is the interval at which the DOM updates to display the calculated frames per second. eg update DOM with current FPS value every 500 ms, rather than every frame.
   */
  constructor(private intervalInMillis: number) {}

  /**
   * Current Frames Per Second
   */
  public FPS: number = 0;

  public prev = performance.now();
  private lastDisplayTime = performance.now();

  /**
   * Call this at your request animation frame tick function
   */
  atTick() {
    const now = performance.now();

    const fps = 1 / ((now - this.prev) / 1000);

    if (now - this.lastDisplayTime >= this.intervalInMillis) {
      this.lastDisplayTime = now;
      this.FPS = Math.round(fps);
    }

    this.prev = now;
  }
}
