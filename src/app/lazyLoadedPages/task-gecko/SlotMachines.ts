import gsap from 'gsap';
import { Spine } from 'pixi-spine';
import {
  Application,
  Container,
  filters,
  Graphics,
  IApplicationOptions,
  ITextStyle,
  Sprite,
  Text,
  TextStyle,
  Texture,
} from 'pixi.js';
import { BehaviorSubject } from 'rxjs';
import { SubscriptionContainer } from 'src/app/helperClasses/subscriptionContainer';
import { dummyResponse } from './dummyJSON';

import { basicReel, spriteReel, spineReel } from './SlotMachineHelpers';
import { SlotMachinePlayer } from './SlotMachinePlayer';

/**
 * Abstract Slot Machine Class to build up on
 */
abstract class _SlotMachine {
  //tweening: tween[] = [];
  // running = false;
  isRunning$ = new BehaviorSubject(false);
  reels: basicReel[] = [];

  subs = new SubscriptionContainer();

  constructor(
    public config: IApplicationOptions,
    public app: Application,
    public reelCount: number,
    public reelRows: number
  ) {}

  /**
   * implement in child classes
   */
  initSlotMachine() {}

  // Reels done handler.
  reelsComplete() {
    // this.running = false;
    this.isRunning$.next(false);
    //  console.warn('done!');
  }

  // Function to start playing.
  startPlay() {
    if (this.isRunning$.getValue()) {
      return;
    }

    this.isRunning$.next(true);
  }
}

/**
 * Basic Slot Machine with Spine Symbols
 */
export class SymbolSlotMachine extends _SlotMachine {
  reels: spineReel[] = [];
  winLineSpines: Spine[] = [];

  REEL_WIDTH = 160;
  SYMBOL_SIZE = 150;

  symbolNameFormatter = new Intl.NumberFormat('en-gb', {
    minimumIntegerDigits: 2,
    useGrouping: false,
  });

  constructor(
    public config: IApplicationOptions,
    public app: Application,
    public reelCount: number,
    public reelRows: number,
    public player: SlotMachinePlayer
  ) {
    super(config, app, reelCount, reelRows);

    this.subs.add = this.player.chosenRandomDummyResponse$.subscribe((data) => {
      console.warn('res', data, this);
      this.swapWithJsonResults(data);
    });
  }

  // finish
  swapWithJsonResults(json: dummyResponse) {
    if (!json) {
      return;
    }

    this.randomizeSpineSymbols();

    this.winLineSpines = [];

    const currentWinLine = this.player.winLines[json.response.results.win ?? 0];

    currentWinLine.forEach((rowIndex, index) => {
      const subConts = this.reels[index].subContainers;

      try {
        subConts[rowIndex + 1].children[0].destroy();

        //  const random = Math.floor(Math.random() * slotSymbolPaths.length);
        //  const path = slotSymbolPaths[random];

        const name = this.symbolNameFormatter.format(
          json.response.results.symbolIDs[index]
        );

        const resource =
          this.app.loader.resources[`assets/symbols/symbol_${name}.json`];
        const updatedSpine = new Spine(resource.spineData);

        updatedSpine.state.setAnimation(1, 'static', true);
        updatedSpine.state.timeScale = 1;

        subConts[rowIndex + 1].addChild(updatedSpine);

        this.winLineSpines.push(updatedSpine);

        // reel.symbols.push(updatedSpine);
      } catch (e) {}
    });

    console.warn('currentWinLine', currentWinLine, '');
  }

  generateRandomSpine() {
    const random = Math.floor(Math.random() * this.slotSymbolPaths.length);
    const path = this.slotSymbolPaths[random];

    const resource = this.app.loader.resources[path];

    const spine = new Spine(resource.spineData!);
    return spine;
  }

  slotSymbolPaths = [
    'assets/symbols/symbol_00.json',
    'assets/symbols/symbol_01.json',
    'assets/symbols/symbol_02.json',
    'assets/symbols/symbol_03.json',
    'assets/symbols/symbol_04.json',
    'assets/symbols/symbol_05.json',
  ];

  initSlotMachine() {
    const app = this.app;

    const REEL_WIDTH = this.REEL_WIDTH;
    const SYMBOL_SIZE = this.SYMBOL_SIZE;

    // Create different slot symbols.

    const slotSymbolPaths = this.slotSymbolPaths;

    // Build the reels

    const reelContainer = new Container();

    for (let i = 0; i < this.reelCount; i++) {
      const reelCol = new Container();

      reelCol.x = i * REEL_WIDTH + REEL_WIDTH / 2;

      reelContainer.addChild(reelCol);

      const reel: spineReel = {
        container: reelCol,
        symbols: [],
        subContainers: [],
        position: 0,
        previousPosition: 0,
        blur: new filters.BlurFilter(),
      };
      reel.blur.blurX = 0;
      reel.blur.blurY = 0;
      reelCol.filters = [reel.blur];

      // Build the symbols
      for (let j = 0; j < this.reelRows * 2; j++) {
        const subCont = new Container();

        subCont.y = j * SYMBOL_SIZE;
        (subCont as any).baseY = j * SYMBOL_SIZE;

        const spine = this.generateRandomSpine();

        // const random = Math.floor(Math.random() * slotSymbolPaths.length);
        //  const path = slotSymbolPaths[random];

        //  const resource = this.app.loader.resources[path];

        // const spine = new Spine(resource.spineData!);
        spine.state.setAnimation(1, 'win', false);
        spine.state.timeScale = 2;

        //  this.app.stage.addChild(spine);
        // this.app.stage.addChild(subCont);

        //  const symbol = new Sprite(slotSymbolPaths[Math.floor(Math.random() * slotSymbolPaths.length)]        );

        // Scale the symbol to fit symbol area.

        reel.symbols.push(spine);
        reel.subContainers.push(subCont);

        subCont.addChild(spine);
        reelCol.addChild(subCont);
      }
      this.reels.push(reel);
    }
    app.stage.addChild(reelContainer);

    /**
     * Top and Bottom Text Containers
     */
    {
      // Build top & bottom covers and position reelContainer
      const margin = (this.config.height! - SYMBOL_SIZE * this.reelRows) / 2;

      reelContainer.y =
        Math.round(this.config.height! - SYMBOL_SIZE * this.reelRows) / 2 - 75; //margin

      reelContainer.x = Math.round(
        this.config.width! / 2 - (REEL_WIDTH * this.reelCount) / 2
      );

      const top = new Graphics();
      top.beginFill(0, 1);
      top.drawRect(0, 0, this.config.width!, margin);
      const bottom = new Graphics();
      bottom.beginFill(0, 1);
      bottom.drawRect(0, SYMBOL_SIZE * 3 + margin, this.config.width!, margin);

      // Add play text
      const baseStyle: Partial<ITextStyle> = {
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
        wordWrapWidth: this.config.width,
      };

      const style = new TextStyle({
        ...baseStyle,
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
      });

      const disabledStyle = new TextStyle({
        ...baseStyle,
        fill: ['#ffffff', '#ffcc6d'], // gradient
        stroke: '#4a1850',
      });

      const playText = new Text('Spin the wheels!', style);

      this.subs.add = this.isRunning$.subscribe((b) => {
        if (b) {
          playText.style = disabledStyle;
          this.player.stakeControl.disable();
        } else {
          playText.style = style;
          this.player.stakeControl.enable();
        }
      });

      playText.x = Math.round((bottom.width - playText.width) / 2);
      playText.y =
        app.screen.height - margin + Math.round((margin - playText.height) / 2);
      bottom.addChild(playText);

      /**
       * Player Balance and Stake Text
       */

      const balanceStyle = new TextStyle({
        fill: ['#ffffff'], // gradient
      });

      // balance
      const balanceText = new Text('Balance: ', balanceStyle);
      balanceText.x = 40;
      balanceText.y = 100;

      // stake
      const stakeText = new Text(
        'Stake: ' + this.player.stakeControl.value,
        balanceStyle
      );
      stakeText.x = 40;
      stakeText.y = 150;

      // last win amount
      const lastWinAmountText = new Text('Win: ', balanceStyle);
      lastWinAmountText.x = 40;
      lastWinAmountText.y = 200;

      top.addChild(balanceText);
      top.addChild(stakeText);
      top.addChild(lastWinAmountText);

      this.subs.add = this.player.balance$.subscribe((b) => {
        balanceText.text = `Balance: ${b}`;
      });
      this.subs.add = this.player.stakeControl.valueChanges.subscribe((b) => {
        stakeText.text = `Stake: ${b}`;
      });
      this.subs.add = this.player.lastWinAmount$.subscribe((b) => {
        lastWinAmountText.text = `Win: ${b}`;
      });
      //balanceText.text = 'wtf?';

      // Add header text
      const headerText = new Text('Firey Fruit Slots!', style);
      headerText.x = Math.round((top.width - headerText.width) / 2);
      headerText.y = Math.round((margin - headerText.height) / 2);
      top.addChild(headerText);

      app.stage.addChild(top);
      app.stage.addChild(bottom);

      // Set the interactivity.
      bottom.interactive = true;
      bottom.buttonMode = true;
      bottom.addListener('pointerdown', () => {
        this.startPlay();
      });
    }
  }

  startSpin() {
    this.reels.forEach((reel, i) => {
      //

      const isLastReel = this.reels.length - 1 == i;

      const x = this.SYMBOL_SIZE * reel.subContainers.length;

      reel.subContainers.forEach((container, j) => {
        //container.y = 213098234908;
        const isLastContainer = reel.subContainers.length - 1 == j;

        gsap.fromTo(
          container,
          { y: container.y },
          {
            y: container.y + x * 2,
            duration: 1,
            delay: i / 30,
            onUpdate: () => {
              //   console.log('?');

              container.y =
                container.y % (this.SYMBOL_SIZE * reel.subContainers.length);
            },
            onComplete: () => {
              if (isLastReel && isLastContainer) {
                this.onSpinComplete();
              }
            },
          }
        );
      });
    });
  }

  startPlay(): void {
    if (this.isRunning$.getValue() === false) {
      this.resetAnimations();

      this.isRunning$.next(true);

      this.player.fetchOnSpin();

      this.startSpin();
    }
    return;

    super.startPlay();
  }

  /**
   * set each reel's spine symbols to a random one
   */
  randomizeSpineSymbols() {
    this.reels.forEach((reel) => {
      reel.subContainers.forEach((cont) => {
        const prevSpine = cont.children[0] as Spine;
        prevSpine.destroy();

        const spine = this.generateRandomSpine();
        cont.addChild(spine);
      });
    });
  }

  resetAnimations() {
    this.reels.forEach((reel) => {
      reel.subContainers.forEach((subCont) => {
        const s = subCont.children[0] as Spine;
        s.state.setAnimation(1, 'static', true);
        s.state.timeScale = 1;
      });
    });
  }

  displayWinAnimations() {
    this.winLineSpines.forEach((spine, index) => {
      if (this.player.matchLength > index && this.player.matchLength > 2) {
        spine.state.setAnimation(1, 'win', false);
        spine.state.timeScale = 1.2;
      }
    });
  }

  onSpinComplete() {
    console.warn('onspincomplete');
    this.reelsComplete();
    this.displayWinAnimations();
  }

  reelsComplete(): void {
    super.reelsComplete();

    console.warn(this.reels);
  }

  destroy() {
    this.app.destroy();
    this.subs.dispose();
  }
}
