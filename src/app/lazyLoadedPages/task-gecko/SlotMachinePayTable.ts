export type payTable = {
  [symbolName: string]: {
    [lineCount: string]: number;
  };
};

/**
 * test coin win amounts per symbol per number of symbols per winline
 */
export const PayTable4x3 = {
  symbol_00: {
    3: 1,
    4: 5,
  },
  symbol_01: {
    3: 2,
    4: 6,
  },
  symbol_02: {
    3: 3,
    4: 7,
  },
  symbol_03: {
    3: 4,
    4: 8,
  },
  symbol_04: {
    3: 5,
    4: 9,
  },
  symbol_05: {
    3: 6,
    4: 10,
  },
};

/**
 * Test win lines for a 4 x 3 grid
 */
export const WinLines4x3: number[][] = [
  [0, 0, 1, 1], // 0

  [0, 0, 0, 0], // 1
  [1, 1, 1, 1], // 2
  [2, 2, 2, 2], // 3

  [0, 1, 1, 0], // 4
  [2, 1, 1, 2], // 5
  [2, 0, 2, 0], // 6

  [0, 2, 0, 2], // 7
  [0, 1, 2, 1], // 8
  [1, 2, 1, 0], // 9
];
