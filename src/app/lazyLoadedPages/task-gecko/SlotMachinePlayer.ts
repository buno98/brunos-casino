import { FormControl } from '@angular/forms';
import { Text } from 'pixi.js';
import { BehaviorSubject } from 'rxjs';
import { SubscriptionContainer } from 'src/app/helperClasses/subscriptionContainer';
import { dummyJSONResponseData, dummyResponse } from './dummyJSON';
import { payTable } from './SlotMachinePayTable';

/**
 * Player class to be used in conjunction with SlotMachine class
 */
export class SlotMachinePlayer {
  lastWinAmount$ = new BehaviorSubject(0);

  public chosenRandomDummyResponse$: BehaviorSubject<dummyResponse> =
    new BehaviorSubject(null);

  subs = new SubscriptionContainer();

  isSlotMachineCalculating$ = new BehaviorSubject(false);

  /**
   * Number showing how many reels have matching symbols, starting from the right;
   */
  matchLength = 0;

  /**
   * Formatting symbol IDs eg 1 to "01"
   */
  symbolNameFormatter = new Intl.NumberFormat('en-gb', {
    minimumIntegerDigits: 2,
    useGrouping: false,
  });

  /**
   *
   * @param balance$ starting balance of the player
   * @param stakeControl formControl for setting stake amount
   * @param winLines win line positions used for displaying the winning symbols from dummy response
   * @param payTable pay table used for caluclating winnings
   * @param maxBet maximum stake amount
   */
  constructor(
    public balance$: BehaviorSubject<number>,
    public stakeControl: FormControl,
    public winLines: number[][],
    public payTable: payTable,
    public maxBet: number
  ) {}

  destroy() {
    this.subs.dispose();
  }

  /**
   * Simulate timeout of fetching response
   */
  async fetchDummyResponse(): Promise<dummyResponse> {
    return new Promise((r) => {
      setTimeout(() => {
        const item =
          dummyJSONResponseData[
            Math.floor(Math.random() * dummyJSONResponseData.length)
          ];
        r(item);
      }, 200);
    });
  }

  hasEnoughToPlayCheck() {
    return this.balance$.getValue() - Number(this.stakeControl.value) >= 0;
  }

  async fetchOnSpin() {
    const canSpin = this.hasEnoughToPlayCheck(); // check if player has enough funds to play

    if (!canSpin) {
      // display error message if cant spin
      alert(
        `Your current balance of "${this.balance$.getValue()}" is not enough to use with "${
          this.stakeControl.value
        }" stake. Please buy more funds, or lower your stake amount.`
      );
      return;
    }

    this.stakeControl.disable();

    this.isSlotMachineCalculating$.next(true);

    const promises: Promise<any>[] = [];

    //  promises.push(this.dummySpinAnimation());

    const item = await this.fetchDummyResponse();
    //this.swapWithJsonResults(item);
    this.chosenRandomDummyResponse$.next(item);

    const winnings = this.calculateWinnings(item);

    this.lastWinAmount$.next(winnings);

    this.balance$.next(
      this.balance$.getValue() - Number(this.stakeControl.value) + winnings
    );

    await Promise.all(promises);

    this.isSlotMachineCalculating$.next(false);

    //    this.stakeControl.enable();
  }

  /**
   * Winnings are calculated as the product of the multiplier shown in the paytable and Stake, divided by the number of win lines
   */
  calculateWinnings(item: dummyResponse) {
    //
    this.matchLength = 0;
    //let matchScore = this.matchScore // payTable amount index

    const stake = Number(this.stakeControl.value); // set by the player

    const symbolIDs = item.response.results.symbolIDs; // dummy response's symbol IDs on the winline

    const firstSymbolID = symbolIDs[0];

    const symbolName =
      'symbol_' + this.symbolNameFormatter.format(firstSymbolID); // formatted with 2 minimumIntegerDigits

    for (let i = 1; i < symbolIDs.length; i++) {
      const currentSymbolId = symbolIDs[i];

      if (currentSymbolId === firstSymbolID) {
        this.matchLength = i + 1; // +1 to match length of win line count;
      } else {
        break;
      }
    }

    const payTableAmount = this.payTable[symbolName]?.[this.matchLength] ?? 0;

    const winnings = payTableAmount * stake; // / matchScore;

    // console.warn({ winnings, matchScore, payTableAmount });

    return winnings || 0;
  }
}
