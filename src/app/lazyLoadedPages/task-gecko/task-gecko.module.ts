import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskGeckoRoutingModule } from './task-gecko-routing.module';
import { TaskGeckoComponent } from './task-gecko.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SlotMachinePageComponent } from './slot-machine-page/slot-machine-page.component';
import { PayTablePageComponent } from './pay-table-page/pay-table-page.component';

@NgModule({
  declarations: [
    TaskGeckoComponent,
    SlotMachinePageComponent,
    PayTablePageComponent,
  ],
  imports: [
    CommonModule,
    TaskGeckoRoutingModule,

    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
  ],
})
export class TaskGeckoModule {}
