import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Application, IApplicationOptions, Loader, Sprite } from 'pixi.js';
import { BehaviorSubject } from 'rxjs';
import { SubscriptionContainer } from 'src/app/helperClasses/subscriptionContainer';
import { PayTable4x3, WinLines4x3 } from '../SlotMachinePayTable';
import { SlotMachinePlayer } from '../SlotMachinePlayer';
import { SymbolSlotMachine } from '../SlotMachines';

@Component({
  selector: 'app-slot-machine-page',
  templateUrl: './slot-machine-page.component.html',
  styleUrls: ['./slot-machine-page.component.css'],
})
export class SlotMachinePageComponent implements OnInit {
  // get DOM ref where Pixi application will be set to
  @ViewChild('appView') appView!: ElementRef;

  // simple application configuration
  config: IApplicationOptions = {
    width: 1080,
    height: 600,
    backgroundColor: 0xff9800,
  };

  app = new Application(this.config);

  // create the player
  player = new SlotMachinePlayer(
    new BehaviorSubject(300),
    new FormControl('20'),
    WinLines4x3,
    PayTable4x3,
    100
  );

  // create the slot machine
  slotMachine = new SymbolSlotMachine(this.config, this.app, 4, 3, this.player);

  currentWinLine: number[]; // for debug purposes (visualising win lines)
  pseudoArray = Array; // for debug purposes (visualising win lines)

  subs = new SubscriptionContainer(); // managing rxjs observables

  constructor() {}

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.initPixiApplication();
  }

  initPixiApplication() {
    this.appView.nativeElement.appendChild(this.app.view); // instead of document.body.appendChild(this.app.view);

    /**
     * Load in all symbols
     */
    const givenSymbolsCount = 6;
    for (let i = 0; i < givenSymbolsCount; i++) {
      const name = this.player.symbolNameFormatter.format(i);
      this.app.loader.add(`assets/symbols/symbol_${name}.json`);
    }

    /**
     * Load background
     */
    this.app.loader.add('assets/images/fireBackground.jpg');

    /**
     * Init Slot Machine after assets loaded
     */
    this.app.loader.onComplete.add((loader) => {
      this.onAssetsLoaded(loader);
    });

    this.app.loader.load();
  }

  onAssetsLoaded(loader: Loader) {
    /**
     * Set up background
     */
    var slide = new Sprite(
      loader.resources['assets/images/fireBackground.jpg'].texture
    );

    this.app.stage.addChild(slide);

    this.slotMachine.initSlotMachine();
  }

  ngOnInit(): void {
    // for debug purposes
    this.player.chosenRandomDummyResponse$.subscribe((value) => {
      this.currentWinLine =
        this.player.winLines[value?.response.results.win ?? 0];
    });
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.player.destroy();
    this.slotMachine.destroy();
    this.subs.dispose();
  }
}
