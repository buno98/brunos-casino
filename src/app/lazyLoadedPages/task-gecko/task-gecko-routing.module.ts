import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PayTablePageComponent } from './pay-table-page/pay-table-page.component';
import { SlotMachinePageComponent } from './slot-machine-page/slot-machine-page.component';
import { TaskGeckoComponent } from './task-gecko.component';

const routes: Routes = [
  { path: '', redirectTo: 'slot-machine', pathMatch: 'full' },
  { path: 'slot-machine', component: SlotMachinePageComponent },
  { path: 'pay-table', component: PayTablePageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaskGeckoRoutingModule {}
