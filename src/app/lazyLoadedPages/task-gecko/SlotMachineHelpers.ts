import { Spine } from 'pixi-spine';
import { Container, Sprite } from 'pixi.js';

export interface basicReel {
  container: Container;
  symbols: any[];
  position: number;
  previousPosition: number;
  blur: any;
}

export interface spriteReel extends basicReel {
  container: Container;
  symbols: Sprite[];
  position: number;
  previousPosition: number;
  blur: any;
}

export interface spineReel extends basicReel {
  symbols: Spine[];

  subContainers: Container[];

  container: Container;

  position: number;
  previousPosition: number;
  blur: any;
}
