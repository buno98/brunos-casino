import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrunoLearningPixieComponent } from './bruno-learning-pixie.component';
import { Lesson1Component } from './lesson1/lesson1.component';
import { Lesson2LoadSymbolsComponent } from './lesson2-load-symbols/lesson2-load-symbols.component';
import { Lesson3SpritesComponent } from './lesson3-sprites/lesson3-sprites.component';
import { Lesson4SpineComponent } from './lesson4-spine/lesson4-spine.component';
import { Lessson5GameLogicComponent } from './lessson5-game-logic/lessson5-game-logic.component';

const routes: Routes = [
  { path: '', redirectTo: 'lesson1', pathMatch: 'full' },
  { path: 'lesson1', component: Lesson1Component },
  { path: 'lesson2', component: Lesson2LoadSymbolsComponent },
  { path: 'lesson3', component: Lesson3SpritesComponent },
  { path: 'lesson4', component: Lesson4SpineComponent },
  { path: 'lesson5', component: Lessson5GameLogicComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrunoLearningPixieRoutingModule {}
