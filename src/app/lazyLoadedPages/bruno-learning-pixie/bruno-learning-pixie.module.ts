import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrunoLearningPixieRoutingModule } from './bruno-learning-pixie-routing.module';
import { BrunoLearningPixieComponent } from './bruno-learning-pixie.component';
import { Lesson1Component } from './lesson1/lesson1.component';
import { Lesson2LoadSymbolsComponent } from './lesson2-load-symbols/lesson2-load-symbols.component';
import { Lesson3SpritesComponent } from './lesson3-sprites/lesson3-sprites.component';
import { Lesson4SpineComponent } from './lesson4-spine/lesson4-spine.component';
import { Lessson5GameLogicComponent } from './lessson5-game-logic/lessson5-game-logic.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
@NgModule({
  declarations: [
    BrunoLearningPixieComponent,
    Lesson1Component,
    Lesson2LoadSymbolsComponent,
    Lesson3SpritesComponent,
    Lesson4SpineComponent,
    Lessson5GameLogicComponent,
  ],
  imports: [
    CommonModule,
    BrunoLearningPixieRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
  ],
})
export class BrunoLearningPixieModule {}
