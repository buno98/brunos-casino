import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { Application, IApplicationOptions, Loader } from 'pixi.js';
import { Spine } from 'pixi-spine';

const formatter = new Intl.NumberFormat('en-gb', {
  minimumIntegerDigits: 2,
  useGrouping: false,
});

@Component({
  selector: 'app-lesson2-load-symbols',
  templateUrl: './lesson2-load-symbols.component.html',
  styleUrls: ['./lesson2-load-symbols.component.css'],
})
export class Lesson2LoadSymbolsComponent implements OnInit {
  @ViewChild('appView') appView!: ElementRef;

  givenSymbolsCount = 5;

  // simple application configuration
  config: IApplicationOptions = {
    width: 1080,
    height: 600,
    backgroundColor: 0xaddad,
  };

  app = new Application(this.config);

  loaderProgressValue = 0;

  constructor() {}

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.app.destroy();
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    this.appView.nativeElement.appendChild(this.app.view); // instead of document.body.appendChild(this.app.view);

    /**
     * Test loading in symbols
     */

    for (let i = 0; i < this.givenSymbolsCount; i++) {
      const name = formatter.format(i);
      this.app.loader.add(`assets/symbols/symbol_${name}.json`);
    }

    //const fireFramesCount = 60;

    this.app.loader.onProgress.add((loader) => {
      this.onProgress(loader);
    });
    this.app.loader.onComplete.add((loader) => {
      this.onAssetsLoaded(loader);
    });

    this.app.loader.load();
  }

  onAssetsLoaded(loader: Loader) {
    console.log('assets loaded', loader);

    this.cherryTest();
    this.allAssetsAnimationTest();
    this.staticAnimationTest();
  }

  onProgress(loader: Loader) {
    // console.log('onProgress', loader);
    this.loaderProgressValue = loader.progress;
  }

  cherryTest() {
    const cherrySymbol =
      this.app.loader.resources['assets/symbols/symbol_00.json'];

    //  const animation = new Spine(cherrySymbol.data);

    const spine = this.app.stage.addChild(new Spine(cherrySymbol.spineData!));
    spine.position.set(this.config.width! * 0.5, this.config.height! * 0.5);
    spine.scale.set(0.5, 0.5);
    //spine.state.setAnimation(0, "action/move-back", true);
    spine.state.timeScale = 0.5;

    console.warn('cherry test!', spine, this.app);
  }

  allAssetsAnimationTest() {
    const baseDistance = this.config.width! / this.givenSymbolsCount;
    const offset = 100;

    for (let i = 0; i < this.givenSymbolsCount; i++) {
      const name = formatter.format(i);
      const resourceName = `assets/symbols/symbol_${name}.json`;

      const symbol = this.app.loader.resources[resourceName];

      const spine = this.app.stage.addChild(new Spine(symbol.spineData!));
      spine.position.set(baseDistance * i + offset, this.config.height! * 0.8);
      spine.scale.set(1);
      //spine.state.setAnimation(0, "action/move-back", true);

      spine.state.setAnimation(0, 'win', true);
      spine.state.timeScale = 1;

      // console.warn('displayAllAssetsTest test!', spine, this.app);
    }
  }

  staticAnimationTest() {
    const baseDistance = this.config.width! / this.givenSymbolsCount;
    const offset = 100;

    for (let i = 0; i < this.givenSymbolsCount; i++) {
      const name = formatter.format(i);
      const resourceName = `assets/symbols/symbol_${name}.json`;

      const symbol = this.app.loader.resources[resourceName];

      const spine = this.app.stage.addChild(new Spine(symbol.spineData!));
      spine.position.set(baseDistance * i + offset, this.config.height! * 0.2);
      spine.scale.set(1);
      //spine.state.setAnimation(0, "action/move-back", true);

      spine.state.setAnimation(0, 'static', true);

      spine.state.timeScale = 1;

      // console.warn('displayAllAssetsTest test!', spine, this.app);
    }
  }
}
