import { Spine } from 'pixi-spine';
import {
  Application,
  Container,
  filters,
  Graphics,
  Sprite,
  Text,
  TextStyle,
  Texture,
} from 'pixi.js';

export interface basicReel {
  container: Container;
  symbols: any[];
  position: number;
  previousPosition: number;
  blur: any;
}

export interface spriteReel extends basicReel {
  container: Container;
  symbols: Sprite[];
  position: number;
  previousPosition: number;
  blur: any;
}

export interface symbolReel extends basicReel {
  symbols: Spine[];

  container: Container;

  position: number;
  previousPosition: number;
  blur: any;
}

// Very simple tweening utility function. This should be replaced with a proper tweening library in a real product.
export type tween = {
  object: any;
  property: any;
  propertyBeginValue: any;
  target: any;
  easing: any;
  time: any;
  change: any;
  complete: any;
  start: number;
};

export function tweenTo(
  object: any,
  property: any,
  target: any,
  time: number,
  easing: any,
  onchange: any,
  oncomplete: any,
  tweening: tween[]
) {
  const tween: tween = {
    object,
    property,
    propertyBeginValue: object[property],
    target,
    easing,
    time,
    change: onchange,
    complete: oncomplete,
    start: Date.now(),
  };

  tweening.push(tween);
  return tween;
}

// Basic lerp funtion.
export function lerp(a1: any, a2: any, t: number) {
  return a1 * (1 - t) + a2 * t;
}

// Backout function from tweenjs.
// https://github.com/CreateJS/TweenJS/blob/master/src/tweenjs/Ease.js
export function backout(amount: number) {
  return (t: number) => --t * t * ((amount + 1) * t + amount) + 1;
}
