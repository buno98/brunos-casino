import { Spine } from 'pixi-spine';
import {
  Application,
  Container,
  filters,
  Graphics,
  IApplicationOptions,
  Sprite,
  Text,
  TextStyle,
  Texture,
} from 'pixi.js';
import {
  backout,
  basicReel,
  lerp,
  spriteReel,
  symbolReel,
  tween,
  tweenTo,
} from './slotTest';

abstract class _SlotMachine {
  tweening: tween[] = [];
  running = false;
  reels: basicReel[] = [];

  constructor(
    public config: IApplicationOptions,
    public app: Application,
    public reelCount: number,
    public reelRows: number
  ) {}

  /**
   * implement in child classes
   */
  initSlotMachine() {}

  // Reels done handler.
  reelsComplete() {
    this.running = false;
  }

  // Function to start playing.
  startPlay() {
    if (this.running) {
      return;
    }
    this.running = true;

    for (let i = 0; i < this.reels.length; i++) {
      const r = this.reels[i];
      const extra = Math.floor(Math.random() * 2);
      const target = r.position + 10 + i * 5 + extra;
      const time = 1000 + i * 60 + extra * 60;

      tweenTo(
        r,
        'position',
        target,
        time,
        backout(0.4),
        null,
        i === this.reels.length - 1
          ? () => {
              this.reelsComplete();
            }
          : null,
        this.tweening
      );
    }
  }
}

export class TestSlotMachine extends _SlotMachine {
  constructor(
    public config: IApplicationOptions,
    public app: Application,
    public reelCount: number,
    public reelRows: number //  private symbolPaths: string[]
  ) {
    super(config, app, reelCount, reelCount);
  }

  initSlotMachine() {
    const app = this.app;

    const REEL_WIDTH = 160;
    const SYMBOL_SIZE = 150;

    // Create different slot symbols.
    const slotTextures = [
      Texture.from('assets/images/lesson3-sprites/0001.png'),
      Texture.from('assets/images/lesson3-sprites/0008.png'),
      Texture.from('assets/images/lesson3-sprites/0013.png'),
      Texture.from('assets/images/lesson3-sprites/0020.png'),
    ];

    // Build the reels

    const reelContainer = new Container();
    for (let i = 0; i < this.reelCount; i++) {
      const rc = new Container();
      rc.x = i * REEL_WIDTH;
      reelContainer.addChild(rc);

      const reel: spriteReel = {
        container: rc,
        symbols: [],
        position: 0,
        previousPosition: 0,
        blur: new filters.BlurFilter(),
      };
      reel.blur.blurX = 0;
      reel.blur.blurY = 0;
      rc.filters = [reel.blur];

      // Build the symbols
      for (let j = 0; j < slotTextures.length; j++) {
        const symbol = new Sprite(
          slotTextures[Math.floor(Math.random() * slotTextures.length)]
        );
        // Scale the symbol to fit symbol area.
        symbol.y = j * SYMBOL_SIZE;
        symbol.scale.x = symbol.scale.y = Math.min(
          SYMBOL_SIZE / symbol.width,
          SYMBOL_SIZE / symbol.height
        );
        symbol.x = Math.round((SYMBOL_SIZE - symbol.width) / 2);
        reel.symbols.push(symbol);
        rc.addChild(symbol);
      }
      this.reels.push(reel);
    }
    app.stage.addChild(reelContainer);

    // Build top & bottom covers and position reelContainer
    const margin = (this.config.height! - SYMBOL_SIZE * this.reelRows) / 2;
    reelContainer.y =
      Math.round(this.config.height! - SYMBOL_SIZE * this.reelRows) / 2; //margin
    reelContainer.x = Math.round(
      (this.config.width! - REEL_WIDTH * this.reelCount) / 2
    );

    const top = new Graphics();
    top.beginFill(0, 1);
    top.drawRect(0, 0, this.config.width!, margin);
    const bottom = new Graphics();
    bottom.beginFill(0, 1);
    bottom.drawRect(0, SYMBOL_SIZE * 3 + margin, this.config.width!, margin);

    // Add play text
    const style = new TextStyle({
      fontFamily: 'Arial',
      fontSize: 36,
      fontStyle: 'italic',
      fontWeight: 'bold',
      fill: ['#ffffff', '#00ff99'], // gradient
      stroke: '#4a1850',
      strokeThickness: 5,
      dropShadow: true,
      dropShadowColor: '#000000',
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 6,
      wordWrap: true,
      wordWrapWidth: this.config.width,
    });

    const playText = new Text('Spin the wheels!', style);
    playText.x = Math.round((bottom.width - playText.width) / 2);
    playText.y =
      app.screen.height - margin + Math.round((margin - playText.height) / 2);
    bottom.addChild(playText);

    // Add header text
    const headerText = new Text('BRUNOS CASINO SLOTS!', style);
    headerText.x = Math.round((top.width - headerText.width) / 2);
    headerText.y = Math.round((margin - headerText.height) / 2);
    top.addChild(headerText);

    app.stage.addChild(top);
    app.stage.addChild(bottom);

    // Set the interactivity.
    bottom.interactive = true;
    bottom.buttonMode = true;
    bottom.addListener('pointerdown', () => {
      this.startPlay();
    });

    // Listen for animate update.
    app.ticker.add((delta) => {
      // Update the slots.
      for (let i = 0; i < this.reels.length; i++) {
        const r = this.reels[i];
        // Update blur filter y amount based on speed.
        // This would be better if calculated with time in mind also. Now blur depends on frame rate.
        r.blur.blurY = (r.position - r.previousPosition) * 8;
        r.previousPosition = r.position;

        // Update symbol positions on reel.
        for (let j = 0; j < r.symbols.length; j++) {
          const s = r.symbols[j];
          const prevy = s.y;
          s.y =
            ((r.position + j) % r.symbols.length) * SYMBOL_SIZE - SYMBOL_SIZE;
          if (s.y < 0 && prevy > SYMBOL_SIZE) {
            // Detect going over and swap a texture.
            // This should in proper product be determined from some logical reel.
            s.texture =
              slotTextures[Math.floor(Math.random() * slotTextures.length)];
            s.scale.x = s.scale.y = Math.min(
              SYMBOL_SIZE / s.texture.width,
              SYMBOL_SIZE / s.texture.height
            );
            s.x = Math.round((SYMBOL_SIZE - s.width) / 2);
          }
        }
      }
    });

    // Listen for animate update.
    app.ticker.add(() => {
      const now = Date.now();
      const remove = [];
      for (let i = 0; i < this.tweening.length; i++) {
        const t = this.tweening[i];
        const phase = Math.min(1, (now - t.start) / t.time);

        t.object[t.property] = lerp(
          t.propertyBeginValue,
          t.target,
          t.easing(phase)
        );
        if (t.change) {
          t.change(t);
        }
        if (phase === 1) {
          t.object[t.property] = t.target;
          if (t.complete) {
            t.complete(t);
          }
          remove.push(t);
        }
      }
      for (let i = 0; i < remove.length; i++) {
        this.tweening.splice(this.tweening.indexOf(remove[i]), 1);
      }
    });
  }
}

export class TestSymbolSlotMachine extends _SlotMachine {
  reels: symbolReel[] = [];

  constructor(
    public config: IApplicationOptions,
    public app: Application,
    public reelCount: number,
    public reelRows: number //  private symbolPaths: string[]
  ) {
    super(config, app, reelCount, reelCount);
  }

  initSlotMachine() {
    const app = this.app;

    const REEL_WIDTH = 160;
    const SYMBOL_SIZE = 150;

    // Create different slot symbols.

    const slotSymbolPaths = [
      'assets/symbols/symbol_00.json',
      'assets/symbols/symbol_01.json',
      'assets/symbols/symbol_02.json',
      'assets/symbols/symbol_03.json',
      'assets/symbols/symbol_04.json',
      'assets/symbols/symbol_05.json',
    ];

    // Build the reels

    const reelContainer = new Container();
    for (let i = 0; i < this.reelCount; i++) {
      const rc = new Container();
      rc.x = i * REEL_WIDTH + REEL_WIDTH / 2;
      reelContainer.addChild(rc);

      const reel: symbolReel = {
        container: rc,
        symbols: [],
        position: 0,
        previousPosition: 0,
        blur: new filters.BlurFilter(),
      };
      reel.blur.blurX = 0;
      reel.blur.blurY = 0;
      rc.filters = [reel.blur];

      // Build the symbols
      for (let j = 0; j < slotSymbolPaths.length; j++) {
        const random = Math.floor(Math.random() * slotSymbolPaths.length);
        const path = slotSymbolPaths[random];

        const resource = this.app.loader.resources[path];

        const spine = this.app.stage.addChild(new Spine(resource.spineData!));

        //  const symbol = new Sprite(slotSymbolPaths[Math.floor(Math.random() * slotSymbolPaths.length)]        );

        // Scale the symbol to fit symbol area.
        spine.y = j * SYMBOL_SIZE;
        spine.scale.x = spine.scale.y = Math.min(
          SYMBOL_SIZE / spine.width,
          SYMBOL_SIZE / spine.height
        );
        spine.x = Math.round((SYMBOL_SIZE - spine.width) / 2);
        reel.symbols.push(spine);
        rc.addChild(spine);
      }
      this.reels.push(reel);
    }
    app.stage.addChild(reelContainer);

    // Build top & bottom covers and position reelContainer
    const margin = (this.config.height! - SYMBOL_SIZE * this.reelRows) / 2;

    reelContainer.y =
      Math.round(this.config.height! - SYMBOL_SIZE * this.reelRows) / 2; //margin

    reelContainer.x = Math.round(
      this.config.width! / 2 - (REEL_WIDTH * this.reelCount) / 2
    );

    const top = new Graphics();
    top.beginFill(0, 1);
    top.drawRect(0, 0, this.config.width!, margin);
    const bottom = new Graphics();
    bottom.beginFill(0, 1);
    bottom.drawRect(0, SYMBOL_SIZE * 3 + margin, this.config.width!, margin);

    // Add play text
    const style = new TextStyle({
      fontFamily: 'Arial',
      fontSize: 36,
      fontStyle: 'italic',
      fontWeight: 'bold',
      fill: ['#ffffff', '#00ff99'], // gradient
      stroke: '#4a1850',
      strokeThickness: 5,
      dropShadow: true,
      dropShadowColor: '#000000',
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 6,
      wordWrap: true,
      wordWrapWidth: this.config.width,
    });

    const playText = new Text('Spin the wheels!', style);
    playText.x = Math.round((bottom.width - playText.width) / 2);
    playText.y =
      app.screen.height - margin + Math.round((margin - playText.height) / 2);
    bottom.addChild(playText);

    // Add header text
    const headerText = new Text('Fire Fruit Slots!', style);
    headerText.x = Math.round((top.width - headerText.width) / 2);
    headerText.y = Math.round((margin - headerText.height) / 2);
    top.addChild(headerText);

    app.stage.addChild(top);
    app.stage.addChild(bottom);

    // Set the interactivity.
    bottom.interactive = true;
    bottom.buttonMode = true;
    bottom.addListener('pointerdown', () => {
      this.startPlay();
    });

    // Listen for animate update.
    app.ticker.add((delta) => {
      // Update the slots.
      for (let i = 0; i < this.reels.length; i++) {
        const r = this.reels[i];
        // Update blur filter y amount based on speed.
        // This would be better if calculated with time in mind also. Now blur depends on frame rate.
        r.blur.blurY = (r.position - r.previousPosition) * 8;
        r.previousPosition = r.position;

        // Update symbol positions on reel.
        for (let j = 0; j < r.symbols.length; j++) {
          let s = r.symbols[j];
          const prevy = s.y;
          s.y =
            ((r.position + j) % r.symbols.length) * SYMBOL_SIZE - SYMBOL_SIZE;
          if (s.y < 0 && prevy > SYMBOL_SIZE) {
            // Detect going over and swap a texture.
            // This should in proper product be determined from some logical reel.

            const random = Math.floor(Math.random() * slotSymbolPaths.length);
            const path = slotSymbolPaths[random];

            const resource = this.app.loader.resources[path];

            //const spine = this.app.stage.addChild(              new Spine(resource.spineData!)            );
            const spine = new Spine(resource.spineData!);

            // console.warn(s);

            s = spine;

            /*
            s.texture =
              this.app.loader.resources[
                slotSymbolPaths[
                  Math.floor(Math.random() * slotSymbolPaths.length)
                ]
              ].texture;
*/
            s.scale.x = s.scale.y = Math.min(
              SYMBOL_SIZE / s.spineData.width,
              SYMBOL_SIZE / s.spineData.height
            );
            s.x = Math.round((SYMBOL_SIZE - s.width) / 2);
          }
        }
      }
    });

    // Listen for animate update.
    app.ticker.add(() => {
      const now = Date.now();
      const remove = [];
      for (let i = 0; i < this.tweening.length; i++) {
        const t = this.tweening[i];
        const phase = Math.min(1, (now - t.start) / t.time);

        t.object[t.property] = lerp(
          t.propertyBeginValue,
          t.target,
          t.easing(phase)
        );
        if (t.change) {
          t.change(t);
        }
        if (phase === 1) {
          t.object[t.property] = t.target;
          if (t.complete) {
            t.complete(t);
          }
          remove.push(t);
        }
      }
      for (let i = 0; i < remove.length; i++) {
        this.tweening.splice(this.tweening.indexOf(remove[i]), 1);
      }
    });
  }
}
