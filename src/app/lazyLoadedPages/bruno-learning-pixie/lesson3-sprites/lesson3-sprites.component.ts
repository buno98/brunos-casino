import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  Application,
  Container,
  filters,
  Graphics,
  IApplicationOptions,
  Loader,
  Sprite,
  Text,
  TextStyle,
  Texture,
} from 'pixi.js';
import { TestSlotMachine } from './TestSlotMachines';

const formatter = new Intl.NumberFormat('en-gb', {
  minimumIntegerDigits: 4,
  useGrouping: false,
});

@Component({
  templateUrl: './lesson3-sprites.component.html',
  styleUrls: ['./lesson3-sprites.component.css'],
})
export class Lesson3SpritesComponent implements OnInit {
  @ViewChild('appView') appView!: ElementRef;

  // simple application configuration
  config: IApplicationOptions = {
    width: 1080,
    height: 600,
    backgroundColor: 0xfaa,
  };

  app = new Application(this.config);

  loaderProgressValue = 0;

  //reelCount = 4;
  //reelRows = 3;

  slotMachine = new TestSlotMachine(this.config, this.app, 6, 3);

  constructor() {}

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.app.destroy();
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    this.appView.nativeElement.appendChild(this.app.view); // instead of document.body.appendChild(this.app.view);

    /**
     * Test loading in frames
     */

    for (let i = 1; i < 21; i++) {
      const name = formatter.format(i);
      this.app.loader.add(`assets/images/lesson3-sprites/${name}.png`);
    }

    this.app.loader.onProgress.add((loader) => {
      this.onProgress(loader);
    });
    this.app.loader.onComplete.add((l) => {
      this.onAssetsLoaded(l);
    });

    this.app.loader.load();
  }

  onAssetsLoaded(loader: Loader) {
    console.log('assets loaded', loader);

    const spriteTest = new Sprite(
      this.app.loader.resources[
        'assets/images/lesson3-sprites/0001.png'
      ].texture
    );

    spriteTest.anchor.set(0.5);
    spriteTest.x = this.config.width! / 2;
    spriteTest.y = this.config.height! / 2;
    spriteTest.scale.set(0.5);

    this.app.stage.addChild(spriteTest);

    this.slotMachine.initSlotMachine();

    // test animated sprite
    let testIndex = 1;
    let testLastMS = 0;
    this.app.ticker.add(() => {
      if (testLastMS + 25 < this.app.ticker.lastTime) {
        testLastMS = this.app.ticker.lastTime;
        const name = formatter.format(testIndex);
        spriteTest.texture =
          this.app.loader.resources[
            `assets/images/lesson3-sprites/${name}.png`
          ].texture!;

        testIndex++;
        if (testIndex > 20) {
          testIndex = 1;
        }
      }
    });
  }

  onProgress(loader: Loader) {
    // console.log('onProgress', loader);
    this.loaderProgressValue = loader.progress;
  }
}
