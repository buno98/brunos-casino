import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Application, IApplicationOptions, Loader } from 'pixi.js';
import { TestSymbolSlotMachine } from '../lesson3-sprites/TestSlotMachines';

const formatter = new Intl.NumberFormat('en-gb', {
  minimumIntegerDigits: 2,
  useGrouping: false,
});

@Component({
  selector: 'app-lesson4-spine',
  templateUrl: './lesson4-spine.component.html',
  styleUrls: ['./lesson4-spine.component.css'],
})
export class Lesson4SpineComponent implements OnInit {
  @ViewChild('appView') appView!: ElementRef;

  // simple application configuration
  config: IApplicationOptions = {
    width: 1080,
    height: 600,
    backgroundColor: 0x006b76,
  };

  givenSymbolsCount = 6;

  app = new Application(this.config);

  slotMachine = new TestSymbolSlotMachine(this.config, this.app, 4, 3);

  constructor() {}

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.app.destroy();
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    this.initPixiApplication();
  }

  initPixiApplication() {
    this.appView.nativeElement.appendChild(this.app.view); // instead of document.body.appendChild(this.app.view);

    /**
     * Test loading in symbols
     */

    for (let i = 0; i < this.givenSymbolsCount; i++) {
      const name = formatter.format(i);
      this.app.loader.add(`assets/symbols/symbol_${name}.json`);
    }

    //const fireFramesCount = 60;

    this.app.loader.onProgress.add((loader) => {
      this.onProgress(loader);
    });
    this.app.loader.onComplete.add((loader) => {
      this.onAssetsLoaded(loader);
    });

    this.app.loader.load();
  }

  onProgress(loader: Loader) {
    // IMPLEMENT
  }
  onAssetsLoaded(loader: Loader) {
    console.warn('assets loaded!', loader);

    this.slotMachine.initSlotMachine();
  }
}
