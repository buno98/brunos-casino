import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import gsap from 'gsap';

import { Application, IApplicationOptions, Sprite } from 'pixi.js';
import { MeasureFPS } from 'src/app/helperClasses/measureFPS';

@Component({
  selector: 'app-lesson1',
  templateUrl: './lesson1.component.html',
  styleUrls: ['./lesson1.component.css'],
})
export class Lesson1Component implements OnInit, AfterViewInit {
  @ViewChild('appView') appView!: ElementRef;

  // simple application configuration
  config: IApplicationOptions = {
    width: 1080,
    height: 600,
    backgroundColor: 0xdadd,
  };

  app = new Application(this.config);

  player!: Sprite;

  measureFPS = new MeasureFPS(500); // performance test

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    // instead of document.body.appendChild(this.app.view);
    if (this.appView) {
      this.appView.nativeElement.appendChild(this.app.view);
    }

    /**
     * Add performance test
     */
    this.app.ticker.add(() => {
      this.measureFPS.atTick();
    });

    /**
     * Player, loaded texture
     */

    this.player = Sprite.from('assets/images/test1.png');
    this.player.anchor.set(0.5);
    this.player.x = this.config.width! / 2;
    this.player.y = this.config.height! / 2;

    this.app.stage.addChild(this.player);

    /**
     * GSAP ANIMATION
     */

    gsap.fromTo(
      this.player,
      { y: 0, duration: 2 },
      { y: this.config.height! / 2, duration: 2, repeat: -1, yoyo: true }
    );

    this.app.ticker.add(() => {
      const elapsedTime = this.app.ticker.elapsedMS;
      this.player.rotation += 0.01;
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

    this.app.destroy();
  }
}
