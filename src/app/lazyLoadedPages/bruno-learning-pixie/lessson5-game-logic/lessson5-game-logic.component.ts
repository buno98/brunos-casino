import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { SubscriptionContainer } from 'src/app/helperClasses/subscriptionContainer';
import {
  testPayTable,
  test4x3WinLines,
  test4x3PayTable,
} from '../lesson3-sprites/bettingLinesTest';
import { DummySlotMachinePlayer } from './playerTest';

@Component({
  selector: 'app-lessson5-game-logic',
  templateUrl: './lessson5-game-logic.component.html',
  styleUrls: ['./lessson5-game-logic.component.css'],
})
export class Lessson5GameLogicComponent implements OnInit {
  player = new DummySlotMachinePlayer(
    new BehaviorSubject(300),
    new FormControl('20'),
    test4x3WinLines,
    test4x3PayTable,
    100
  );

  currentWinLine: number[] = null;

  subs = new SubscriptionContainer();

  pseudoArray = Array;

  constructor() {}

  ngOnDestroy(): void {
    this.subs.dispose();
    this.player.destroy();
  }

  ngOnInit(): void {
    this.player.chosenRandomResponse$.subscribe((value) => {
      this.currentWinLine =
        this.player.winLines[value?.response.results.win ?? 0];
    });
  }
}
