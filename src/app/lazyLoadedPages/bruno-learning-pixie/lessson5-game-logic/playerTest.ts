import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { SubscriptionContainer } from 'src/app/helperClasses/subscriptionContainer';
import { testPayTable } from '../lesson3-sprites/bettingLinesTest';

const symbolIdFormatter = new Intl.NumberFormat('en-gb', {
  minimumIntegerDigits: 2,
  useGrouping: false,
});

type response = {
  response: {
    results: {
      win: number;
      symbolIDs: number[];
    };
  };
};

//Dummy JSON responses
let data: response[] = [
  {
    response: {
      results: {
        win: 0,
        symbolIDs: [2, 5, 2, 1],
      },
    },
  },

  {
    response: {
      results: {
        win: 8,
        symbolIDs: [5, 5, 5, 1],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [0, 3, 1, 4],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [5, 4, 1, 1],
      },
    },
  },

  {
    response: {
      results: {
        win: 2,
        symbolIDs: [1, 1, 5, 3],
      },
    },
  },

  {
    response: {
      results: {
        win: 4,
        symbolIDs: [2, 2, 2, 3],
      },
    },
  },

  {
    response: {
      results: {
        win: 4,
        symbolIDs: [5, 5, 2, 2],
      },
    },
  },

  {
    response: {
      results: {
        win: 3,
        symbolIDs: [2, 2, 3, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [4, 5, 3, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 8,
        symbolIDs: [5, 5, 5, 3],
      },
    },
  },

  {
    response: {
      results: {
        win: 9,
        symbolIDs: [3, 3, 3, 3],
      },
    },
  },

  {
    response: {
      results: {
        win: 6,
        symbolIDs: [4, 4, 4, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 1,
        symbolIDs: [0, 0, 3, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 5,
        symbolIDs: [1, 1, 1, 2],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [2, 5, 2, 2],
      },
    },
  },

  {
    response: {
      results: {
        win: 5,
        symbolIDs: [2, 2, 2, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [4, 3, 0, 5],
      },
    },
  },

  {
    response: {
      results: {
        win: 6,
        symbolIDs: [3, 3, 3, 0],
      },
    },
  },

  {
    response: {
      results: {
        win: 8,
        symbolIDs: [2, 2, 2, 2],
      },
    },
  },

  {
    response: {
      results: {
        win: 0,
        symbolIDs: [0, 1, 5, 4],
      },
    },
  },
];

/**
 * Test class for developing player class for the final task
 */
export class DummySlotMachinePlayer {
  public chosenRandomResponse$: BehaviorSubject<response> = new BehaviorSubject(
    null
  );

  subs = new SubscriptionContainer();

  isSlotMachineRunning$ = new BehaviorSubject(false);

  constructor(
    public balance$: BehaviorSubject<number>,
    public stakeControl: FormControl,
    public winLines: number[][],
    public payTable: testPayTable,
    public maxBet: number
  ) {}

  destroy() {
    this.subs.dispose();
  }

  async fetchDummyResponse(): Promise<response> {
    return new Promise((r) => {
      setTimeout(() => {
        const item = data[Math.floor(Math.random() * data.length)];
        r(item);
      }, 800);
    });
  }

  async dummySpinAnimation() {
    return new Promise((r) => {
      setTimeout(() => {
        r(null);
      }, Math.random() * 500);
    });
  }

  hasEnoughToSpinCheck() {
    return this.balance$.getValue() - Number(this.stakeControl.value) >= 0;
  }

  async onSpin() {
    const canSpin = this.hasEnoughToSpinCheck(); // check if player has enough funds to play

    if (!canSpin) {
      // display error message
      alert(
        `Your current balance of "${this.balance$.getValue()}" is not enough to use with "${
          this.stakeControl.value
        }" stake. Please buy more funds, or lower your stake amount.`
      );
      return;
    }

    //   this.stakeControl.disable();

    this.isSlotMachineRunning$.next(true);

    const promises: Promise<any>[] = [];

    promises.push(this.dummySpinAnimation());

    const item = await this.fetchDummyResponse();
    this.chosenRandomResponse$.next(item);

    const winnings = this.calculateWinnings(item);

    this.balance$.next(
      this.balance$.getValue() - Number(this.stakeControl.value) + winnings
    );

    await Promise.all(promises);

    this.isSlotMachineRunning$.next(false);

    // this.stakeControl.enable();
  }

  /**
   * Winnings are calculated as the product of the multiplier shown in the paytable and Stake, divided by the number of win lines
   */
  calculateWinnings(item: response) {
    //
    const stake = Number(this.stakeControl.value); // set by the player

    const symbolIDs = item.response.results.symbolIDs; // dummy response's symbol IDs on the winline

    const firstSymbolID = symbolIDs[0];

    const symbolName = 'symbol_' + symbolIdFormatter.format(firstSymbolID); // formatted with 2 minimumIntegerDigits

    let matchScore = 0; // payTable amount index

    for (let i = 1; i < symbolIDs.length; i++) {
      const currentSymbolId = symbolIDs[i];

      if (currentSymbolId === firstSymbolID) {
        matchScore = i + 1; // +1 to match length of win line count;
      } else {
        break;
      }
    }

    const payTableAmount = this.payTable[symbolName]?.[matchScore] ?? 0;

    const winnings = payTableAmount * stake; // / matchScore;

    // console.warn({ winnings, matchScore, payTableAmount });

    return winnings || 0;
  }
}
